/*
      $(function () {
        $('#btnOK').click(function () {


            $.get('homework3_1.json', function(jsonResult) {
                console.log(jsonResult);
                $('#result').append("name:" + jsonResult[0]['name']);
            });


        });
    });
    */
   $.get('homework3_1.json', function(data) {
        //$('#result').append("id:" + data[0]['id']);
        

        let str_result = "<table>";
        str_result+="<tr>";
        for(let field in data[0]){
            str_result += "<td>"+field+"</td>";
        }
        str_result+="</tr>";

        for(let count in data){
            str_result+="<tr>";
            for(let field in data[count]){
                str_result += "<td>"+data[count][field]+"</td>";
            }
            str_result+="</tr>";
        }
      
        str_result += "</table>";

      $('#result').append(str_result);

      for(let count in data){
        addAdditionalFields(data[count]);
      }

        let str_result1 = "</br></br></br></br><table>";
        str_result1+="<tr>";
        for(let field in data[0]){
            str_result1 += "<td>"+field+"</td>";
        }
        str_result1+="</tr>";

        for(let count in data){
            str_result1+="<tr>";
            for(let field in data[count]){
                if(field=="nextSalary"){
                    str_result1 += "<td><ol>";
                    for(let salary in data[count]["nextSalary"]){
                        str_result1 += "<li>"+data[count]["nextSalary"][salary] +"</li>";
                    }
                    str_result1 += "</ol></td>";
                }else{
                    str_result1 += "<td>"+data[count][field]+"</td>";
                }
            }
            str_result1+="</tr>";
        }
      
        str_result1 += "</table>";

        $('#result').append(str_result1);
    });
    
    function addYearSalary(row) {
        row['salary'] = parseInt(row['salary']);
        row['yearSalary'] = row['salary']*12;
    } 
    function addNextSalary(row) {
        year1 = parseInt(row['salary']);
        year2 = parseInt(year1+((year1*10)/100));
        year3 = parseInt(year2+((year2*10)/100));
        row["nextSalary"]=[year1,year2,year3];
    }
    function addAdditionalFields(row){
        addYearSalary(row);
        addNextSalary(row);
    }