
   $.get('homework3_1.json', function(Employees) {
      let newEmployees = cloneArray(Employees);

      for(let key in Employees){
        addAdditionalFields(newEmployees[key]);
      }
      newEmployees[0]['salary'] = 0;
      let str_result = "<table>";
      str_result+="<tr>";
      for(let field in Employees[0]){
          str_result += "<td>"+field+"</td>";
      }
      str_result+="</tr>";

      for(let count in Employees){
          str_result+="<tr>";
          for(let field in Employees[count]){
              if(field=="nextSalary"){
                  str_result += "<td><ol>";
                  for(let salary in Employees[count]["nextSalary"]){
                      str_result += "<li>"+Employees[count]["nextSalary"][salary] +"</li>";
                  }
                  str_result += "</ol></td>";
              }else{
                  str_result += "<td>"+Employees[count][field]+"</td>";
              }
          }
          str_result+="</tr>";
      }
    
      
      str_result += "</table>";
      $('#result').append(str_result);

        let str_result1 = "</br></br></br></br><table>";
        str_result1+="<tr>";
        for(let field in newEmployees[0]){
            str_result1 += "<td>"+field+"</td>";
        }
        str_result1+="</tr>";

        for(let count in newEmployees){
            str_result1+="<tr>";
            for(let field in newEmployees[count]){
                if(field=="nextSalary"){
                    str_result1 += "<td><ol>";
                    for(let salary in newEmployees[count]["nextSalary"]){
                        str_result1 += "<li>"+newEmployees[count]["nextSalary"][salary] +"</li>";
                    }
                    str_result1 += "</ol></td>";
                }else{
                    str_result1 += "<td>"+newEmployees[count][field]+"</td>";
                }
            }
            str_result1+="</tr>";
        }
      
        str_result1 += "</table>";

        $('#result').append(str_result1);

    });
    function cloneArray(varVal){
        let newArray = new Array();
        for(let key in varVal){
            if(typeof(varVal)!="object"){
                newArray[key] = varVal[key];
            }else{
                newArray[key]=new Object();
                for(let key1 in varVal[key]){
                    if(typeof(varVal[key][key1])!="object"){
                        newArray[key][key1] = varVal[key][key1];
                    }else{
                        newArray[key][key1]=new Array();
                        for(let key2 in varVal[key][key1]){
                            if(typeof(varVal[key][key1][key2])!="object"){
                                newArray[key][key1][key2] = varVal[key][key1][key2];
                            }else{
                                
                            }
                        }
                    }
                }
            }
        }
        return newArray;
        /*
        varVal.isArray();
        console.log(typeof(varVal));
        */

    }
    
    function addYearSalary(row) {
        row['salary'] = parseInt(row['salary']);
        row['yearSalary'] = row['salary']*12;
    } 
    function addNextSalary(row) {
        year1 = parseInt(row['salary']);
        year2 = parseInt(year1+((year1*10)/100));
        year3 = parseInt(year2+((year2*10)/100));
        row["nextSalary"]=[year1,year2,year3];
    }
    function addAdditionalFields(row){
        addYearSalary(row);
        addNextSalary(row);
    }