class PassingArrayToLoop {
    public static void main(String[] args) {
      int[] myArray = { 
        { "1", "2", "3" }, 
        { "4", "5", "6" }, 
        { "7", "8", "9" } 
        };
      printArray(myArray);
    }
    public static void printArray(int[] _array) {
      for (int item: _array) {
        System.out.println(item);
      }
    }
   }
   