class ArrayLength {
    public static void main(String[] g){
      int[] intArray = new int [5];
      char[] charArray = {'A', 'B', 'C', 'D'};
      String[] StringArray = {"Dog", "Cat"};
      System.out.println("intArray length: " + intArray.length);
      System.out.println("charArray length: " + charArray.length);
      System.out.println("StringArray length: " + StringArray.length);
    }
   }
   