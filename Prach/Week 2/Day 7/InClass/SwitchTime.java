class SwitchTime {
    public static void main(String[] args) {
      int mealTime = 3;
      String mealString = "";
      switch (mealTime) {
      case 1:
        mealString = "1: Breakfast"; break;
      case 2:
        mealString = "2: Lunch"; break;
      case 3:
        mealString = "3: Dinner"; break;
      case 4:
        mealString = "4: Snack"; break;
      default:
        System.out.println("Invalid meal time!");
      }
      System.out.println(mealString);
    }
   }
   