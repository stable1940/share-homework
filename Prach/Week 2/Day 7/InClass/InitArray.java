class InitArray {
    public static void main(String[] args) {
      int[] myIntArray = new int[5];
      myIntArray[0] = 10;
      myIntArray[1] = 30;
      myIntArray[2] = 80;
      myIntArray[3] = 5;
      myIntArray[4] = 25;
      for (int item : myIntArray) {
        System.out.println("Item :" + item);
      }
    }
   }
   