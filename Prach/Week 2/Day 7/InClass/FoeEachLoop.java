class ForEachLoop {
    public static void main(String[] args) {
      int myArray[] = {1,2,3,4,5};
      // like  for(let value in);
      // ต่างจาก JS ตรงไม่หมุน key แต่หมุน value
      for(int counter : myArray)  {
        System.out.println("Counter :" + counter);
      }
    }
   }
   