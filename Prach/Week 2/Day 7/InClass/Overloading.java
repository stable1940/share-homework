class Overloading {
    public static void main(String[] args) {
      int a = 11, b = 10;
      double c = 7.2, d = 8.1;
      int minInt = findMin(a, b);
      double minDouble = findMin(c, d);
      System.out.println("minInt :" + minInt);
      System.out.println("minDouble :" + minDouble);
    }
    public static int findMin(int n1, int n2) {
        int min;
        if (n1 > n2 ) min = n1;
        else min = n2;
        return min;
       }
       public static double findMin(double n1, double n2) {
        double min;
        if (n1 > n2 ) min = n1;
        else min = n2;
        return min;
       }
      }
      