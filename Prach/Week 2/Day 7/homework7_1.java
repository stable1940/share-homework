class homework7_1 {
  public static void main(String[] args) {
    draw8(2);
  };
  // draw1
  public static void draw1(int num1) {
    for(int i=0; i<num1; i++) {
      System.out.print("*");
    }
    System.out.println();
  }   
  // draw2
  public static void draw2(int num2) {
    for(int j=0; j<num2; j++) {
      for(int i=0; i<num2; i++) {
        System.out.print("*");
      }
      System.out.println();
    };  
  }
  // draw3
  public static void draw3(int num3) {
    
    for (int i=0; i<num3; i++){
      int kk = 0;
      for(int j=0; j<num3; j++){        
        kk += 1;
        System.out.print(kk);
      }
      System.out.println();
    }
  }
  // draw4
  public static void draw4(int num4) {
    for (int i=0; i<num4; i++){
      for(int j=num4; j>0; j--){        
        System.out.print(j);
      }
      System.out.println();
    }
  }
  // draw5
  public static void draw5(int num5) {
    for (int i=0; i<num5; i++){
      for(int j=0; j<num5; j++){        
        System.out.print(i+1);
      }
      System.out.println();
    }
  }
  // draw6
  public static void draw6(int num6) {
    for (int i=num6; i>0; i--){
      for(int j=num6; j>0; j--){        
        System.out.print(i);
      }
      System.out.println();
    }
  }
  // draw7
  public static void draw7(int num7) {
    int k=0;
    for (int i=0; i<num7; i++){
      for(int j=0; j<num7; j++){ 
        k+=1;       
        System.out.print(k);
      }
      System.out.println();
    }
  }
  //draw8
  public static void draw8(int num8) {
    int k=num8*num8;
    for (int i=0; i<num8; i++){
      for(int j=0; j<num8; j++){               
        System.out.print(k);
        k--;
      }
      System.out.println();
    }
  }
};
 
 