import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;

class homework8_1 {
  public static void main(String[] args) {
    String[] rawData = {
      "id:1001 firstname:Luke lastname:Skywalker",
      "id:1002 firstname:Tony lastname:Stark",
      "id:1003 firstname:Somchai lastname:Jaidee",
      "id:1004 firstname:MonkeyD lastname:Luffee"
    };
    ArrayList<HashMap<String, String>> Arr = new ArrayList<>();

      for(int i= 0 ; i < rawData.length ; i++){

      String[] splitString = rawData[i].split(" ");
      ArrayList<String> mySplitItem = new ArrayList<>(Arrays.asList(splitString));
      // System.out.println(mySplitItem);
 
        HashMap<String,String> Hash = new HashMap<>();

        for(String element : mySplitItem){
            String[] key = element.split(":");
            // System.out.println(key[0]);
            // System.out.println(key[1]);
            Hash.put(key[0],key[1]);
        }
        // System.out.println(Hash);
        Arr.add(Hash);
    }
    // System.out.println(Arr);

    for(HashMap<String, String> value : Arr){
      for(String id : value.keySet() ){
        System.out.println( id + " : " + value.get(id));
      }
    }

    

  }
}
