class homework1{
    public static void main(String[] args) {

        System.out.println("draw1");
        draw1(2);
        draw1(3);
        draw1(4);

        System.out.println("draw2");
        draw2(2);
        draw2(3);
        draw2(4);

        System.out.println("draw3");
        draw3(2);
        draw3(3);
        draw3(4);

        System.out.println("draw4");
        draw4(2);
        draw4(3);
        draw4(4);

        System.out.println("draw5");
        draw5(2);
        draw5(3);
        draw5(4);

        System.out.println("draw6");
        draw6(2);
        draw6(3);
        draw6(4);

        System.out.println("draw7");
        draw7(2);
        draw7(3);
        draw7(4);

        System.out.println("draw8");
        draw8(2);
        draw8(3);
        draw8(4);
    }

    public static void draw1(int d1){
        for(int i = 0 ; i < d1; i++){
            System.out.print("*");
        }
        System.out.println();
    }

    public static void draw2(int d2){
        for (int l = 0 ; l < d2 ; l++){

            for(int i = 0 ; i < d2; i++){
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println();
        
    }

    public static void draw3(int d3){
        for (int l = 0 ; l < d3 ; l++){

            for(int i = 1 ; i <= d3; i++){
                System.out.print(i);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void draw4(int d4){
        for (int l = 0 ; l < d4 ; l++){

            for(int i = d4 ; i > 0; i--){
                System.out.print(i);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void draw5(int d5){
        for (int l = 1 ; l <= d5 ; l++){
            
            for(int i = 0 ; i < d5; i++){
                System.out.print(l);
            }
            System.out.println();
        }
        System.out.println();
        
    }

    public static void draw6(int d6){
        for(int l = d6 ; l > 0 ; l--){

            for(int i = 0 ; i < d6 ; i++){
                System.out.print(l);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void draw7(int d7){

        int n = 0;
        for(int l = 0 ; l < d7 ; l++){

            for(int i = 0 ; i < d7 ; i++){
                n++;
                System.out.print(n);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void draw8(int d8){

        int n = d8*d8;
        
        for(int l = 0 ; l < d8 ; l++){

            for(int i = 0 ; i < d8 ; i++){
                System.out.print(n);
                n--;
            }
            System.out.println();
        }
        System.out.println();
    }

}