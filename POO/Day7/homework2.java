class homework2{
    public static void main(String[] args) {
        String[][] table = { 
            { "1", "2", "3","5"}, 
            { "4", "5", "6" }, 
            { "7", "8", "9" } 
       }; 
       multiplyTable(table);
    }


    public static void multiplyTable(String[][] table){
        for (String[] row : table) {
            int l = 1;
            for (String element: row) {
                
                int i = Integer.parseInt(element);
                System.out.print(i * 2);
                if(l != row.length){
                    System.out.print(",");
                }
              l++;
            }
            System.out.println();
        }
    }

}
