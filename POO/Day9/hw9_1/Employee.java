package hw9_1;

public class Employee {
    public   String firstname;
    public   String lastname;
    private  int salary;
    public static String planet = "Earth";
    public static int freeChocolateLeft = 20;
    public String dressCode;

    
   
    public Employee(String firstnameInput, String lastnameInput, int salaryInput) {
        this.firstname = firstnameInput;
        this.lastname = lastnameInput;
        this.salary = salaryInput;
    }
    public void hello() {
        System.out.println("Hello " + this.firstname );
    }
    public int getSalary() {
        return salary;
    }

    public void setSalary(int setNewSalary){
        this.salary = setNewSalary;
    }

    public void gossip(Employee one,String two){
        System.out.println("Hey " + one.firstname + ", " + two);
    }

}

